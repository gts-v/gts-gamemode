import * as alt from 'alt';
import * as native from 'natives';

var roundTime = undefined;
var roundTimeModifier = 0;
var specialCam = undefined;
var hideHud = false;
var aliveTeamMembers = [];
var aliveTeamBlips = [];
var currentlySpectating = false;
var killFeedList = [];
var updatingTeamMembers = false;

const screenRes = native.getActiveScreenResolution(0, 0);

alt.onServer('setRoundTime', setRoundTime); // Called to set the current round time until finished.
alt.onServer('setupCamera', setupCamera); // Called to setup the player camera for weapon selection.
alt.onServer('aliveTeamMembers', setAliveTeamMembers); // Set the alive players for a team.
alt.onServer('enableSpectateMode', enableSpectateMode); // Enable spectator mode.
alt.onServer('killFeed', killFeed); // Push kills and deaths to the kill feed.

alt.on('update', () => {
    // Restore Stamina
    native.restorePlayerStamina(alt.Player.local.scriptID, 100);

    // Display Round Time to the user.
    if (roundTime !== undefined && !hideHud) {
        drawText(`Time Left: ${millisToMinutesAndSeconds((roundTime + roundTimeModifier) - Date.now())}`, 0.5, 0.01, 0.5, 255, 255, 255, 100);
    }

    // Switch Players While Spectating
    if (currentlySpectating !== false) {
        if (native.isControlJustPressed(0, 24)) {
            let firstPlayer = aliveTeamMembers.shift();
            aliveTeamMembers.push(firstPlayer);

            if (aliveTeamMembers.length >= 1) {
                native.requestCollisionAtCoord(alt.Player.local.pos.x, alt.Player.local.pos.y, alt.Player.local.pos.z)
                native.networkSetInSpectatorMode(true, aliveTeamMembers[0].scriptID);
            }
        }

        if (aliveTeamMembers[0] !== undefined) {
            drawText('Spectating', 0.5, 0.2, 0.5, 255, 255, 255, 100);
            drawText(`${aliveTeamMembers[0].name}`, 0.5, 0.25, 0.5, 255, 255, 255, 100);
        }
    }

    // Display Kill Feed
    if (killFeedList.length >= 1) {
        killFeedList.forEach((killDetails, index) => {
            if (killDetails.team === 'red') {
                drawText(`~r~${killDetails.victim}~w~ was killed by ~b~${killDetails.attacker}`, 0.85, 0.05 + (0.05 * index), 0.5, 255, 255, 255, 250 - (index * 50));
            } else {
                drawText(`~b~${killDetails.victim}~w~ was killed by ~r~${killDetails.attacker}`, 0.85, 0.05 + (0.05 * index), 0.5, 255, 255, 255, 250 - (index * 50));
            }
        });
    }

    // Draw Name Tags
    if (aliveTeamMembers.length >= 1) {
        aliveTeamMembers.forEach((member) => {
            if (member === alt.Player.local)
                return;

            const pos = alt.Player.local.pos;
            const targetPos = member.pos;

            if (Distance(pos, targetPos) <= 20) {
                let [_result, _x, _y] = native.getScreenCoordFromWorldCoord(targetPos.x, targetPos.y, targetPos.z + 1.25, undefined, undefined)

                if (_result) {
                    _y -= 0.4 * (0.005 * (screenRes[2] / 1080));

                    drawText(`${member.name}`, _x, _y, 0.4, 255, 255, 255, 150);
                }
            }
        });
    }

    // Update Blip Positions
    if (!updatingTeamMembers && aliveTeamBlips.length >= 1) {
        aliveTeamBlips.forEach((memberInfo) => {
            memberInfo.blip.position = [memberInfo.member.pos.x, memberInfo.member.pos.y, memberInfo.member.pos.z];
        });
    }
});

function setRoundTime(newRoundTime, newRoundModifier) {
    if (newRoundTime === null || newRoundTime === undefined) {
        roundTime = undefined;
        roundTimeModifier = 0;
        return;
    }

    roundTime = newRoundTime;
    roundTimeModifier = newRoundModifier;
    //playAudio('roundstart');
}

// Draw text; must be called in update function.
function drawText(msg, x, y, scale, r, g, b, a) {
    native.setUiLayer(50);
    native.beginTextCommandDisplayText('STRING');
    native.addTextComponentSubstringPlayerName(msg);
    native.setTextFont(4);
    native.setTextScale(1, scale);
    native.setTextWrap(0.0, 1.0);
    native.setTextCentre(true);
    native.setTextColour(r, g, b, a);
    native.setTextOutline();
    native.endTextCommandDisplayText(x, y)
}

// Calculate milliseconds to minutes; and display as string.
function millisToMinutesAndSeconds(millis) {
    var minutes = Math.floor(millis / 60000);
    var seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}

// Update alive team members for nametags, and blips.
function setAliveTeamMembers(players, teamColor) {
    aliveTeamMembers = players;
    currentTeamColor = teamColor;

    if (updatingTeamMembers)
        return;

    updatingTeamMembers = true;
    if (aliveTeamBlips.length >= 1) {
        while (aliveTeamBlips.length >= 1) {
            var data = aliveTeamBlips.pop();

            if (data.blip !== undefined) {
                data.blip.destroy();
            }
        }
    }

    alt.log('created first round of blips.');

    aliveTeamMembers.forEach((member) => {
        let newBlip = new alt.PointBlip(member.pos.x, member.pos.y, member.pos.z);
        newBlip.sprite = 1;

        if (teamColor === 'red') {
            newBlip.color = 1;
        } else {
            newBlip.color = 3;
        }

        aliveTeamBlips.push({ member, blip: newBlip });
    });
    updatingTeamMembers = false;
}

// Enabled spectator mode.
function enableSpectateMode() {
    currentlySpectating = true;
    native.doScreenFadeOut(1000);

    alt.setTimeout(() => {
        // Requires request collision at coord; to show the players.
        native.requestCollisionAtCoord(alt.Player.local.pos.x, alt.Player.local.pos.y, alt.Player.local.pos.z)
        native.doScreenFadeIn(1000);

        if (aliveTeamMembers[0] !== undefined) {
            native.networkSetInSpectatorMode(true, aliveTeamMembers[0].scriptID);
        }
    }, 3000);
}

// Display 4 Kills / Deaths
function killFeed(victim, attacker, team) {
    killFeedList.unshift({ victim, attacker, team });

    if (killFeedList.length >= 5) {
        killFeedList.pop();
    }
}

// Setup a camera that points at the capture point.
function setupCamera(camPos) {
    currentlySpectating = false;
    if (camPos === undefined)
        return;

    specialCam = native.createCamWithParams("DEFAULT_SCRIPTED_CAMERA", camPos.x + 3, camPos.y + 3, camPos.z + 5, 0, 0, 0, 90, true, 0);
    native.pointCamAtCoord(specialCam, camPos.x, camPos.y, camPos.z)
    native.renderScriptCams(true, false, 0, true, false);
}

// Clear any special cameras.
function clearCamera() {
    specialCam = undefined;
    native.destroyAllCams(false);
    native.renderScriptCams(false, false, 0, false, false);
}
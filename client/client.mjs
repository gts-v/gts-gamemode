import * as alt from 'alt';
import * as account from './cef/login.mjs';
import * as notify from './cef/notify.mjs';
import * as arena from './arena.mjs';
import * as lobby from './cef/lobby.mjs';
jQuery(document).ready(function() {

    /*
        Fullscreen background
    */

    $.backstretch([
        "assets/img/backgrounds/backgrou2nd.jpg"
    ], { duration: 0, fade: 0 });


    /*
        Form validation
    */
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
        $(this).removeClass('input-error');
    });

    $(".register").on("click", function() {
        $("#login").css("display", "none");
        $("#register").css("display", "block");
    });

    $(".login").on("click", function() {
        $("#register").css("display", "none");
        $("#login").css("display", "block");
    });

    $('.login-form').on('submit', function(e) {

        $(this).find('input[type="text"], input[type="password"], textarea').each(function() {
            if ($(this).val() == "") {
                e.preventDefault();
                $(this).addClass('input-error');
            } else {
                $(this).removeClass('input-error');
            }
        });

    });


});
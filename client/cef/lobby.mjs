import * as alt from 'alt';
import * as native from 'natives';

var lobby = undefined;

alt.onServer('createLobbyCEF', () => {

    alt.showCursor(true);
    native.displayHud(false);
    native.displayRadar(false);
    alt.toggleGameControls(false);

    lobby = new alt.WebView('http://resources/gts-gamemode/client/cef/lobby/index.html'); //Lobby
    lobby.focus();

    lobby.on("sendLobbyname", (lobby) => {
        alt.emitServer("GTS-LobbyJoin", lobby);
        alt.log('GTS-LobbyJoin > ' + lobby);
        destroyLobbyBrowser();
    });

});

let destroyLobbyBrowser = function() {
    if (lobby != null) {
        lobby.destroy();
    }
    lobby = null;
    native.transitionFromBlurred(0);
    alt.showCursor(false);
    alt.toggleGameControls(true);
    native.displayRadar(true);
    native.freezeEntityPosition(alt.Player.local.scriptID, false);
    native.setEntityAlpha(alt.Player.local.scriptID, 255, 0);
}
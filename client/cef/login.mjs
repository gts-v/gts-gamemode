import * as alt from 'alt';
import * as native from 'natives';

var accountWeb = undefined;

alt.onServer('createLoginCEF', (state) => {

    alt.showCursor(true);
    native.displayHud(false);
    native.displayRadar(false);
    alt.toggleGameControls(false);

    if (state) {

    } else {
        accountWeb = new alt.WebView('http://resources/gts-gamemode/client/cef/login/index.html'); //Login
        accountWeb.focus();
    }

    accountWeb.on("sendToLogin", (user, pass) => {
        alt.emitServer("GTS-Login", user, pass);
        //destroyLoginBrowser();
    });

    accountWeb.on("sendToRegister", (user, pass) => {
        alt.emitServer("GTS-Register", user, pass);
        //destroyLoginBrowser();
    });

    accountWeb.on("destroyWindow", () => {
        destroyLoginBrowser();
    });

    /*  accountWeb.emit("loginExceptionToCef", () => {
        alt.emit("loginExceptionToCef", reason);
        alt.log("Exeception: " + reason)
    }); */

    alt.onServer("credentialError", (reason) => {
        accountWeb.emit("credentialError", reason)
    });

});


alt.onServer('destroyLogin', () => {
    destroyLoginBrowser();
});

let destroyLoginBrowser = function() {
    if (accountWeb != null) {
        accountWeb.destroy();
    }
    accountWeb = null;
    native.transitionFromBlurred(0);
    alt.showCursor(false);
    alt.toggleGameControls(true);
    native.displayRadar(true);
    native.freezeEntityPosition(alt.Player.local.scriptID, false);
    native.setEntityAlpha(alt.Player.local.scriptID, 255, 0);
}
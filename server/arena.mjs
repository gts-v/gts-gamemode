import * as alt from 'alt';
import chat from 'chat';
import { pool } from './mysql.mjs';

const modelsToUse = ['a_m_m_afriamer_01', 'u_m_y_baygor'];
const redTeam = []; // Red Team Members
const blueTeam = []; // Blue Team Members
var unassignedPlayers = []; // Players who die are assigned to this array.
var captureEnterTime = undefined; // The time when the player enters the capture point.
var roundStartTime = Date.now(); // Set the round start time; changes frequently.
var roundTimeModifier = 300000; // 5 Minutes
var restartingRound = false;

alt.on('startJoinArena', (player) => {
    chat.broadcast("startJoinArena fired")
    console.log("startJoinArena fired")
    player.setDateTime(1, 1, 1, 12, 0, 0);
    player.setWeather(8);
    alt.emitClient(player, 'setRoundTime', roundStartTime, roundTimeModifier);
    addToTeam(player);
    alt.emitClient(player, "createNotify", "GWWW!");
});

alt.on('playerDisconnect', (player) => {
    removeFromTeam(player);
});

// Called when a player is eliminated from the match.
alt.on('playerDeath', (victim, attacker, weapon) => {
    unassignedPlayers.push(victim);
    removeFromTeam(victim);

    if (attacker !== null && attacker.constructor.name === "Player") {}

    if (victim.constructor.name === "Player" && attacker !== null && attacker.constructor.name === "Player") {
        alt.emitClient(null, 'killFeed', victim.name, attacker.name, victim.team);
        alt.emitClient(victim, 'enableSpectateMode');
    }

    // Red Team Dies
    if (redTeam.length <= 0) {
        resetRound();
        chat.broadcast(`Blue team has won the round.`);
        return;

    }

    if (blueTeam.length <= 0) {
        resetRound();
        chat.broadcast(`Red team has won the round.`);
        return;
    }
});

alt.on('chatIntercept', (player, msg) => {
    if (player.team === 'red')
        chat.broadcast(`{FF0000}${player.name} {FFFFFF}: ${msg}`);
    else
        chat.broadcast(`{0000FF}${player.name} {FFFFFF}: ${msg}`);
});

// ushort actualDamage = 65536 - damage;
alt.on('playerDamage', (victim, attacker, damage, weapon) => {
    const actualDamage = 65536 - damage;
    if (attacker === null) {
        return;
    }

    if (victim.constructor.name !== "Player" || attacker.constructor.name !== "Player") {
        return;
    }

    if (attacker.team === undefined) {
        return;
    }

    // Prevent team killing.
    if (victim.team === attacker.team) {
        victim.health += actualDamage;
        return;
    }
});


// load weapons for a player
alt.onClient('loadWeapons', (player, weaponHashes) => {
    player.removeAllWeapons();

    var parsedArray = JSON.parse(weaponHashes);

    if (!Array.isArray(parsedArray))
        return;

    parsedArray.forEach((wep) => {
        player.giveWeapon(wep, 999, true);
    });
});

/**
 * Autoselect a team for a player.
 * @param player 
 */
function addToTeam(player) {
    if (redTeam.length <= blueTeam.length) {
        player.team = 'red';
        redTeam.push(player);
        handleRedSpawn(player);

        redTeam.forEach((redMember) => {
            if (redMember === player) {
                chat.send(redMember, `You have joined the {FF0000}red team`);
                return;
            }

            chat.send(redMember, `${player.name} has joined your {FF0000}team`);
        });
    } else {
        player.team = 'blue';
        blueTeam.push(player);
        resetRound(player);

        blueTeam.forEach((blueMember) => {
            if (blueMember === player) {
                chat.send(blueMember, `You have joined the {0000FF}blue team`);
                return;
            }

            chat.send(blueMember, `${player.name} has joined your {0000FF}team`);
        });
    }

    updateAlivePlayers();
}

/**
 * Auto remove a player from a team.
 * @param player 
 */
function removeFromTeam(player) {
    if (player.constructor.name === "Player") {
        if (player.team === 'red') {
            //var pos = utility.RandomPosAround(redTeamSpawn, 3);
            //player.pos = pos;
        } else {
            //var pos = utility.RandomPosAround(capturePoint, 3);
            //player.pos = pos;
        }
    }

    if (player.team === 'red') {
        let index = redTeam.findIndex(x => x === player);
        if (index !== -1) {
            redTeam.splice(index, 1);
            updateTeams();
        }
    }

    if (player.team === 'blue') {
        let index = blueTeam.findIndex(x => x === player);

        if (index !== -1) {
            blueTeam.splice(index, 1);
            updateTeams();
        }
    }
}

/**
 * Called to spawn a red team member.
 * @param player 
 */
function handleRedSpawn(player) {
    //var pos = utility.RandomPosAround(redTeamSpawn, 5);
    //player.spawn(pos.x, pos.y, pos.z, 100);
    player.model = modelsToUse[1]; // Ballas Model
    player.health = 200;
    //WAFFENGEBEN
}

/**
 * Called to spawn a blue team member.
 * @param player 
 */
function handleBlueSpawn(player) {
    //var pos = utility.RandomPosAround(capturePoint, 5);
    //player.spawn(pos.x, pos.y, pos.z, 100);
    player.model = modelsToUse[0]; // Grove Model
    player.health = 200;
    //WAFFEN
}

/**
 * Update the client-side team member array; to display their current
 * team members.
 */
function updateTeams() {
    if (redTeam.length >= 1) {
        redTeam.forEach((redMember) => {
            if (redMember.constructor.name !== "Player")
                return;

            alt.emitClient(redMember, 'setTeamMembers', redTeam);
        });
    }

    if (blueTeam.length >= 1) {
        blueTeam.forEach((blueMember) => {
            if (blueMember.constructor.name !== "Player")
                return;

            alt.emitClient(blueMember, 'setTeamMembers', blueTeam);
        });
    }

    unassignedPlayers.forEach((player) => {
        if (player.constructor.name !== "Player")
            return;

        if (player.team === 'red') {
            alt.emitClient(player, 'aliveTeamMembers', redTeam, 'red');
        } else {
            alt.emitClient(player, 'aliveTeamMembers', blueTeam, 'blue');
        }
    });

    updateAlivePlayers();
}

/**
 * Update alive players with their currently available team.
 */
function updateAlivePlayers() {
    blueTeam.forEach((player) => {
        if (player.constructor.name !== "Player")
            return;

        alt.emitClient(player, 'aliveTeamMembers', blueTeam, 'blue');
    });

    redTeam.forEach((player) => {
        if (player.constructor.name !== "Player")
            return;

        alt.emitClient(player, 'aliveTeamMembers', redTeam, 'red');
    });
}

function shuffle(array) {
    let counter = array.length;

    while (counter > 0) {
        let index = Math.floor(Math.random() * counter);

        counter--;

        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}


// Called to reset the round; and start a new one.
function resetRound() {
    if (restartingRound)
        return;

    restartingRound = true;

    roundStartTime = undefined;
    alt.emitClient(null, 'setRoundTime', undefined);
    alt.emitClient(null, 'setupCamera', capturePoint);
    captureEnterTime = undefined;

    alt.Player.all.forEach((target) => {
        if (target.constructor.name !== "Player")
            return;

        removeFromTeam(target);
    });

    // Clear red team.
    while (redTeam.length >= 1) {
        removeFromTeam(redTeam.pop());
    }

    // Clear blue team.
    while (blueTeam.length >= 1) {
        removeFromTeam(blueTeam.pop());
    }

    // Change Map
    var firstMap = captureRotation.shift();
    captureRotation.push(firstMap);

    redTeamSpawn = captureRotation[0].redTeamSpawn;
    capturePoint = captureRotation[0].capturePoint;

    // Round Reset
    setTimeout(() => {

        roundStartTime = Date.now();
        alt.emitClient(null, 'setRoundTime', roundStartTime, roundTimeModifier);

        var lastShuffle = alt.Player.all;
        for (var i = 0; i < 10; i++) {
            lastShuffle = shuffle(lastShuffle);
        }

        lastShuffle.forEach((target) => {
            if (target.constructor.name !== "Player")
                return;

            addToTeam(target);
        });

        unassignedPlayers = [];
        restartingRound = false;
    }, 5000);
}

// Weather / Date Updater
setInterval(() => {
    alt.Player.all.forEach((player) => {
        if (player.constructor.name !== "Player")
            return;

        player.setDateTime(1, 1, 1, 12, 0, 0);
        player.setWeather(8);
    });
}, 60000);

// Round Timer
setInterval(() => {
    if (roundStartTime === undefined)
        return;

    if (Date.now() < roundStartTime + roundTimeModifier)
        return;

    resetRound();
    chat.broadcast(`Unentschieden.`);
}, 5000);
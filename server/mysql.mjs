import mysql from 'mysql';

var con = {
    connectionLimit: 20,
    host: 'localhost',
    user: 'gts',
    password: 'gts',
    database: 'gts',
    stringifyObjects: true
};

export var pool = mysql.createPool(con);
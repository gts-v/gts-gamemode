import * as alt from 'alt';
import chat from 'chat';

import('./mysql.mjs');
import('./auth.mjs');
import('./players.mjs');
import('./arena.mjs');
import('./lobbysystem.mjs');

alt.on('playerConnect', (player) => {

    player.model = 's_m_y_airworker';
    player.spawn(813, -279, 66, 1000);
    alt.emitClient(player, 'createLoginCEF', false);

});

alt.on('playerDisconnect', (player, reason) => {
    //saveAccount(player);
});

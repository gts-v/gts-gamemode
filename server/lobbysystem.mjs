import * as alt from 'alt';
import { pool } from './mysql.mjs';


alt.onClient('GTS-LobbyJoin', (player, lobby) => {
    pool.query('SELECT `currentArena` FROM `accounts` WHERE `socialclub` = ?', [player.socialId], function(err1, res1) {
        pool.query(`'UPDATE currentArena = ? WHERE socialclub = ?`, [lobby, player.socialId], function(err, res) {
            switch (lobby) {
                case 'Arena':
                    alt.emitClient(player, "createNotify", "Du spielst nun Arena Fights");
                    alt.emit('startJoinArena', player);
                    break;
                case 'GW':
                    alt.emitClient(player, "createNotify", "Du spielst nun Gangwar");

                    break;
                case 'FFA':
                    alt.emitClient(player, "createNotify", "Du spielst nun Free for All");
                    break;
                default:
                    console.log("No Lobby?");
                    break;
            }
        });
    });
});

alt.onClient('GTS-LobbyLeave', (player) => {
    var lobby = "None";
    pool.query('SELECT `currentArena` FROM `accounts` WHERE `socialclub` = ?', [player.socialId], function(err1, res1) {
        pool.query(`'UPDATE currentArena = ? WHERE socialclub = ?`, [lobby, player.socialId], function(err, res) {
            // Send to Lobby
        });
    });
});
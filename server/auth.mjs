import * as alt from 'alt';
import { pool } from './mysql.mjs';
import bcrypt from 'bcrypt-nodejs';

export function loadingAccount(player) {
    pool.query(`SELECT * FROM accounts WHERE socialclub = ?`, [player.socialId], function(err, res, row) {
        if (err) console.log(err);
        //alt.emitClient(player, "LobbySystem");
        //ACC DETAILS WERDEN GELADEN
    });

};

export function saveAccount(player) {
    var level = 1;
    var xp = 1;
    var kills = 1;
    var deaths = 1;
    pool.query(`'UPDATE xp = ?, level = ?, kills = ?, deaths = ? WHERE socialclub = '${player.socialId}'`, [xp, level, kills, deaths], function(err, res, row) {
        if (err) console.log(err);
        if (res.length) {
            res.forEach(function(sql) {
                // DATEN SPEICHERUNG
            });
        }
    });
};

alt.onClient('GTS-Register', (player, username, pass) => {

    if (username.length >= 3 && pass.length >= 5) {
        pool.query('SELECT * FROM `accounts` WHERE `socialclub` = ?', [player.socialId], function(err, res) {
            pool.query('SELECT * FROM `accounts` WHERE `username` = ?', [username], function(err1, res1) {
                if (res.length > 0) {
                    alt.emitClient(player, 'credentialError', "Dieser Social Club ist bereits bei uns gemeldet.");
                    if (res1.length > 0) {
                        alt.emitClient(player, 'credentialError', "Dieser Benutzername wird bereits verwendet.");
                        return;
                    }
                } else {
                    bcrypt.hash(pass, null, null, function(err, hash) {
                        if (!err) {
                            pool.query('INSERT INTO `accounts` SET username = ?, socialclub  = ?, password  = ?, hwid = ?, lastip = ?', [username, player.socialId, hash, player.hwidHash, player.ip], function(err, res) {
                                //SEND TO LOBBY
                                alt.emitClient(player, 'DestroyLogin');
                                alt.emitClient(player, "createNotify", "Erfolgreich regestriert!");
                            });
                        } else {
                            console.log('Fehler beim Password verschlüsseln: ' + err);
                        }
                    });
                }
            });
        });
    } else {
        alt.emitClient(player, 'credentialError', "Bitte beachte das dein Benutzername mindestens aus 3 Zeichen bestehen muss.");
    }
});

alt.onClient('GTS-Login', (player, username, pass) => {
    let loggedAccount = alt.Player.all.find(p => p.loggedInAs == player.socialId);
    if (loggedAccount) {
        alt.log(`${player.name} bereits eingeloggt.!`);
    } else {
        pool.query('SELECT `password`, `hwid` FROM `accounts` WHERE `socialclub` = ?', [player.socialId], function(err, res) {
            let hwid = res[0]['hwid'];
            if (hwid === player.hwidHash) {

                let passsword = res[0]['password'];
                bcrypt.compare(pass, passsword, function(err, res2) {
                    if (res2 === true) {
                        pool.query(`'UPDATE lastip = ? WHERE socialclub = ?`, [player.ip, player.socialId], function(err, res) {
                            alt.emitClient(player, 'destroyLogin');
                            alt.emitClient(player, "createLobbyCEF");
                            //startJoinArena(player);
                            //loadingAccount(player);
                            //alt.emitClient(player, 'DestroyLogin');
                        });
                    } else {
                        alt.emitClient(player, 'credentialError', "Falsches Passwort!");
                    }

                });
            } else {
                alt.emitClient(player, 'credentialError', "Falsche HWID melde dich im Support!");
            }
        });
    }
});